from django.http import HttpResponse
from django.shortcuts import render, redirect
from commandes.models import *
from commandes.forms import *
from passlib.hash import pbkdf2_sha256
import base64
from PIL import Image
from io import BytesIO, StringIO

def index(request):
    return render(request, 'index.html')

def list_utilisateurs(request):
    admin = request.GET.get('admin')
    if admin=='true':
        utilisateurs = Utilisateur.objects.filter(admin=True)
    elif admin=='false':
        utilisateurs = Utilisateur.objects.filter(admin=False)
    else:
        utilisateurs = Utilisateur.objects.all()
    return render(request, 'utilisateurs.html', {'utilisateurs': utilisateurs})

def list_commandes(request):
    commandes = Commande.objects.all()
    return render(request, 'commandes.html', {'commandes': commandes})

def create_utilisateur(request):
    form = UtilisateurForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('list_utilisateurs')
   
    return render(request, 'utilisateur-form.html', {'form': form})

def create_commande(request):
    form = CommandeForm(request.POST or None)

    if form.is_valid():
        form=form.save()
        return redirect('list_commandes')
   
    return render(request, 'commande-form.html', {'form': form})

def show_utilisateur_commande(request, id):
    utilisateur_commandes= Commande.objects.filter(utilisateur= id)
    utilisateur= Utilisateur.objects.get(id=id)
    return render(request, 'commandes.html', {'commandes': utilisateur_commandes,'utilisateur':utilisateur})

def update_utilisateur(request, id):
    utilisateur = Utilisateur.objects.get(id=id)
    form = UtilisateurForm(request.POST or None, instance=utilisateur)

    if form.is_valid():
        form.save()
        return redirect('list_utilisateurs')
   
    return render(request, 'utilisateur-form.html', {'form': form, 'utilisateur': utilisateur})

def update_commande(request, id):
    commande= Commande.objects.get(id=id)
    form = CommandeForm(request.POST or None, instance=commande)

    if form.is_valid():
        form.save()
        return redirect('list_commandes')
   
    return render(request, 'commande-form.html', {'form': form, 'commande': commande})

def delete_utilisateur(request, id):
    utilisateur = Utilisateur.objects.get(id=id)
    
    if request.method =='POST':
        utilisateur.delete()
        return redirect('list_utilisateurs')
    
    return render(request, 'delete-confirm.html', {'utilisateur': utilisateur})

def delete_commande(request, id):
    commande = Commande.objects.get(id=id)

    if request.method =='POST':
        commande.delete()
        return redirect('list_commandes')

    return render(request, 'delete-confirm.html', {'commande': commande})

def show_image(request, id):
    commande = Commande.objects.get(id=id)
    pic=BytesIO()
    img=BytesIO(base64.b64decode(commande.image))
    image=Image.open(img)
    image.save(pic, image.format)
    pic.seek(0)
    return HttpResponse(pic,content_type='image/'+image.format)
