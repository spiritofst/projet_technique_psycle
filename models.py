from django.db import models
from passlib.hash import pbkdf2_sha256

class Utilisateur(models.Model):
    username = models.CharField(max_length=128,unique= True)
    password = models.CharField(max_length=128)
    admin =  	models.BooleanField()
    
    def __str__(self):
        return self.username
    
    def est_un_admin(self):
        return self.admin
    est_un_admin.boolean = True
    
    def save(self, *args, **kwargs):
        self.password=pbkdf2_sha256.encrypt(self.password,rounds=10000,salt_size=32)
        super(Utilisateur, self).save(*args, **kwargs)

    def verifier_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)

class Commande(models.Model):
    objet = models.CharField(max_length=128)
    prix = models.DecimalField(max_digits=15, decimal_places=2)
    image = models.TextField()
    utilisateur = models.ForeignKey(Utilisateur , on_delete=models.PROTECT)

    def __str__(self):
        return str(self.id)
