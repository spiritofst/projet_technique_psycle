from django.contrib import admin

from .models import Utilisateur,Commande

class UtilisateurAdmin(admin.ModelAdmin):
    fields = ['username', 'password', 'admin']
    list_display = ('username', 'est_un_admin')
    list_filter = ['admin']

class CommandeAdmin(admin.ModelAdmin):
    fields =['objet', 'prix', 'image', 'utilisateur']

admin.site.register(Utilisateur, UtilisateurAdmin)
admin.site.register(Commande, CommandeAdmin)
