from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('utilisateurs', views.list_utilisateurs, name='list_utilisateurs'),
    path('commandes', views.list_commandes, name='list_commandes'),
    path('utilisateur/new', views.create_utilisateur, name='create_utilisateur'),
    path('commande/new', views.create_commande, name='create_commande'),
    path('utilisateur/update/<int:id>/', views.update_utilisateur, name='update_utilisateur'),
    path('utilisateur/<int:id>/commandes', views.show_utilisateur_commande, name='show_utilisateur_commande'),
    path('commande/update/<int:id>/', views.update_commande, name='update_commande'),
    path('utilisateur/delete/<int:id>/', views.delete_utilisateur, name='delete_utilisateur'),
    path('commande/delete/<int:id>/', views.delete_commande, name='delete_commande'),
    path('commande/<int:id>/image', views.show_image, name='show_image')
]